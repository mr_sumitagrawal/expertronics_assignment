/*
 *   Copyright (c) 2019 Sumit Agrawal
 *   All rights reserved.

 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at

 *   http://www.apache.org/licenses/LICENSE-2.0

 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
var mongoose = require("mongoose");

module.exports.create = (request, response) => {
	if (!request.body.name || !request.body.expert_email || !request.body.task) {
		console.log("one or more parameters missing.");
		console.log("body: " + JSON.stringify(request.body));
		response.status(400).end();
	} else {
		var tasks = mongoose.model("tasks");
		tasks.findOne(
			{
				name: request.body.name
			},
			(error, result) => {
				if (error) {
					console.log("ERROR: " + JSON.stringify(error));
					response.status(401).end();
					return;
				}
				console.log("RESULT: " + JSON.stringify(result));
				if (result != null) response.status(406).end();
				else {
					tasks.insertMany(
						{
							name: request.body.name,
							expert_email: request.body.expert_email,
							task: request.body.task
						},
						(error, result) => {
							if (error) {
								console.log("ERROR: " + JSON.stringify(error));
								response.status(406).end();
								return;
							}
							console.log(result);
							response.status(200).end();
						}
					);
				}
			}
		);
	}
};

module.exports.update = (request, response) => {
	if (
		!request.body.old_name ||
		!request.body.name ||
		!request.body.expert_email ||
		!request.body.task
	) {
		console.log("one or more parameters missing");
		console.log("body: " + JSON.stringify(request.body));
		response.status(400).end();
	} else {
		var tasks = mongoose.model("tasks");
		tasks.updateMany(
			{
				name: request.body.old_name
			},
			{
				name: request.body.name,
				expert_email: request.body.expert_email,
				task: request.body.task
			},
			(error, result) => {
				if (error) {
					console.log("ERROR: " + JSON.stringify(error));
					response.status(406).end();
					return;
				}
				console.log(result);
				response.status(200).end();
			}
		);
	}
};

module.exports.read = (request, response) => {
	var query = request.query;
	var tasks = mongoose.model("tasks");
	tasks.find(
		query,
		{ _id: 0, __v: 0, createdAt: 0, updatedAt: 0 },
		(error, result) => {
			if (error) {
				console.log("ERROR: " + JSON.stringify(error));
				response.status(406).end();
				return;
			}
			response.status(200).send(result);
		}
	);
};

module.exports.remove = (request, response) => {
	var tasks = mongoose.model("tasks");
	tasks.deleteOne({ name: request.body.name }, (error, result) => {
		if (error) {
			console.log("ERROR: " + JSON.stringify(error));
			response.status(406).end();
			return;
		}
		if (result.deletedCount <= 0) response.status(406).end();
		else response.status(200).end();
	});
};
