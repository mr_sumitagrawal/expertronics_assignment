/*
 *   Copyright (c) 2019 Sumit Agrawal
 *   All rights reserved.

 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at

 *   http://www.apache.org/licenses/LICENSE-2.0

 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
const jwt = require("jsonwebtoken");
var mongoose = require("mongoose");

module.exports.login = (request, response) => {
	if (!request.body.email || !request.body.password || !request.body.type) {
		console.log("one or more parameters missing. login failed");
		console.log("body: " + JSON.stringify(request.body));
		response.status(400).end();
	} else {
		var users = mongoose.model("users");
		users.findOne(
			{
				email: request.body.email,
				password: request.body.password,
				type: request.body.type
			},
			(error, result) => {
				if (error) {
					console.log("ERROR: " + JSON.stringify(error));
					response.status(401).end();
					return;
				}
				console.log(JSON.stringify(result));
				if (result == null) response.status(401).end();
				else {
					jwt.sign(
						{
							data: {
								email: request.body.email,
								password: request.body.password,
								type: request.body.type
							}
						},
						process.env.SECRET,
						{ expiresIn: "1h" },
						(err, token) => {
							console.log(token);
							response.status(200).send({
								email: result.email,
								token: token,
								type: result.type
							});
						}
					);
				}
			}
		);
	}
};
