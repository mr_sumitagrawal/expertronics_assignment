var mongoose = require('mongoose');

var usersSchema = new mongoose.Schema({
    email: { type: String, lowercase: true, unique: true, required: [true, "can't be blank"], index: true },
    password: String,
    name:String,
    mobile:String,
    type:String,
    dob:{ type: String, default: '' },
    gender:{ type: String, default: '' },
    home_town:{ type: String, default: '' },
    job_sector:{ type: String, default: '' },
    company_name:{ type: String, default: '' },
    designation:{ type: String, default: '' },
    posting_place:{ type: String, default: '' },
    ctc:{ type: String, default: '' },
    shortlisted_ccompanies:{ type: String, default: '' },
    internship_experience:{ type: String, default: '' },
    college_name:{ type: String, default: '' },
    programme:{ type: String, default: '' },
    department:{ type: String, default: '' },
    cgpa:{ type: String, default: '' },
    ssc:{ type: String, default: '' },
    hsc:{ type: String, default: '' },
    academic_project:{ type: String, default: '' },
    leadership_roles:{ type: String, default: '' },
    extra_curricular:{ type: String, default: '' },
    description:{ type: String, default: '' },
    linkedin_profile:{ type: String, default: '' }
}, { timestamps: true });

module.exports = mongoose.model('users', usersSchema);