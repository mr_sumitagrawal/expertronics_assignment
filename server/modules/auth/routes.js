/*
 *   Copyright (c) 2019 Sumit Agrawal
 *   All rights reserved.

 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at

 *   http://www.apache.org/licenses/LICENSE-2.0

 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
const express = require('express');
const router = express.Router();
const login = require('./controllers/login')
const users = require('./controllers/users')

require('./model/mongo/')

router.post('/login', login.login);

//users routes
router.post('/users', users.create);
router.put('/users', users.update);
router.get('/users', users.read);
router.delete('/users', users.remove);

module.exports = router;
