/*
 *   Copyright (c) 2019 Sumit Agrawal
 *   All rights reserved.

 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at

 *   http://www.apache.org/licenses/LICENSE-2.0

 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
const jwt = require("jsonwebtoken");
var mongoose = require("mongoose");

module.exports.authenticate = (request, response, next) => {
	if (request.path != "/login") {
		jwt.verify(
			request.headers.authorization.split(" ")[1],
			process.env.SECRET,
			(err, decoded) => {
				console.log("err: " + JSON.stringify(err));
				console.log("decoded: " + JSON.stringify(decoded));
				if (decoded === undefined) {
					console.log("decoded undefined AUTHENTICATION failed");
					console.log("AUTHENTICATION Un-successful");
					response.status(406).end();
				} else {
					var users = mongoose.model("users");
					users.findOne(
						{
							email: decoded.data.email,
							password: decoded.data.password
						},
						(error, result) => {
							if (error) {
								console.log("ERROR: " + JSON.stringify(error));
								response.status(406).end();
								return;
							}
							console.log(JSON.stringify(result));
							if (result == null) response.status(406).end();
							else {
								next();
							}
						}
					);
				}
			}
		);
	} else {
		next();
	}
};
