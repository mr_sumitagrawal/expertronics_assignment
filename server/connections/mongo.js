/*
 *   Copyright (c) 2019 Sumit Agrawal
 *   All rights reserved.

 *   Licensed under the Apache License, Version 2.0 (the "License");
 *   you may not use this file except in compliance with the License.
 *   You may obtain a copy of the License at

 *   http://www.apache.org/licenses/LICENSE-2.0

 *   Unless required by applicable law or agreed to in writing, software
 *   distributed under the License is distributed on an "AS IS" BASIS,
 *   WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *   See the License for the specific language governing permissions and
 *   limitations under the License.
 */
var mongoose = require("mongoose");

const MONGO_HOST = process.env.MONGO_HOST || "localhost";
const MONGO_PORT = process.env.MONGO_PORT;
const MONGO_DB = process.env.MONGO_DB || "expertrons_db";
const MONGO_URL = "mongodb://" + MONGO_HOST + ":" + MONGO_PORT + "/" + MONGO_DB;

mongoose.connect(MONGO_URL, {
	useNewUrlParser: true,
	useUnifiedTopology: true
});
const db = mongoose.connection;
db.on("error", error => {
	console.error("MongoDB connection error:", error);
});

db.once("open", () => {
	console.info("MongoDB connection is established.");
});

db.on("disconnected", error => {
	console.error("MongoDB disconencted!");
	console.error("ERROR: ", error);
});

db.on("reconnected", () => {
	console.info("MongoDB reconnected!");
});
