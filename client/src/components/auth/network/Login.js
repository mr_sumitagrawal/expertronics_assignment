import superagent from "superagent";

const URL = "/api/v1/login";

export const login = (credentials, callback) => {
	superagent
		.post(URL)
		.send(credentials)
		.end((err, response) => {
			if (err) {
				callback(null);
				console.log("Request failed");
			} else {
				if (response.status == 200) {
					callback(response.body);
				} else {
					callback(null);
				}
			}
			console.log(response);
		});
};
