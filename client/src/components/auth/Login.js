import React from "react";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import CssBaseline from "@material-ui/core/CssBaseline";
import TextField from "@material-ui/core/TextField";
import LockOutlinedIcon from "@material-ui/icons/LockOutlined";
import Typography from "@material-ui/core/Typography";
import { withStyles } from "@material-ui/core/styles";
import Container from "@material-ui/core/Container";
import LoadingDialog from "../commons/LoadingDialog";
import Select from "@material-ui/core/Select";
import MenuItem from "@material-ui/core/MenuItem";
import InputLabel from "@material-ui/core/InputLabel";
import { login } from "./network/Login";

const styles = theme => ({
	"@global": {
		body: {
			backgroundColor: theme.palette.common.white
		}
	},
	paper: {
		marginTop: theme.spacing(8),
		display: "flex",
		flexDirection: "column",
		alignItems: "center"
	},
	avatar: {
		margin: theme.spacing(1),
		backgroundColor: theme.palette.secondary.main
	},
	form: {
		width: "100%",
		marginTop: theme.spacing(1)
	},
	submit: {
		margin: theme.spacing(3, 0, 2)
	}
});

class Login extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			isLoading: false,
			isDone: false,
			email: "",
			password: "",
			loadingMessage: "Verifying credentials, Please wait",
			status: 0
		};
		this.handleSubmit = this.handleSubmit.bind();
		this.onLoadingDone = this.onLoadingDone.bind();
	}

	handleSubmit = event => {
		event.preventDefault();
		this.setState({ isLoading: true });
		this.setState({ isDone: false });

		login(
			{
				email: this.state.email,
				password: this.state.password,
				type: this.state.type
			},
			result => {
				if (result != null) {
					console.log(result);
					this.setState({ status: 200 });
					this.setState({ email: this.state.email });
					this.setState({ loadingMessage: "Login Done" });
					this.setState({ token: result.token });
					this.setState({ type: result.type });
					this.setState({ isDone: true });
				} else {
					this.setState({ status: 400 });
					this.setState({ loadingMessage: "Login failed" });
					this.setState({ isDone: true });
				}
			}
		);
	};

	onLoadingDone = event => {
		this.setState({ isLoading: false });
		if (this.state.status === 200) {
			this.props.onLogin({
				email: this.state.email,
				token: this.state.token,
				type: this.state.type
			});
		}
	};

	render() {
		const { classes } = this.props;
		return (
			<Container component="main" maxWidth="xs">
				<LoadingDialog
					open={this.state.isLoading}
					status={this.state.isDone}
					message={this.state.loadingMessage}
					onDone={this.onLoadingDone}
				/>
				<CssBaseline />
				<div className={classes.paper}>
					<Avatar className={classes.avatar}>
						<LockOutlinedIcon />
					</Avatar>
					<Typography component="h1" variant="h5">
						Sign in
					</Typography>
					<form className={classes.form} noValidate>
						<TextField
							variant="outlined"
							margin="normal"
							required
							fullWidth
							type="email"
							onChange={event => this.setState({ email: event.target.value })}
							id="email"
							label="email"
							autoComplete="email"
							autoFocus
						/>
						<TextField
							variant="outlined"
							margin="normal"
							required
							fullWidth
							onChange={event =>
								this.setState({ password: event.target.value })
							}
							label="Password"
							type="password"
							id="password"
							autoComplete="current-password"
						/>
						<InputLabel htmlFor="type">User type</InputLabel>
						<Select
							id="type"
							margin="normal"
							variant="outlined"
							fullWidth
							onChange={event => this.setState({ type: event.target.value })}
						>
							<MenuItem value={"admin"}>Admin</MenuItem>
							<MenuItem value={"expert"}>Expert</MenuItem>
							<MenuItem value={"student"}>Student</MenuItem>
						</Select>
						<Button
							type="submit"
							fullWidth
							variant="contained"
							color="primary"
							onClick={this.handleSubmit}
							className={classes.submit}
						>
							Login
						</Button>
					</form>
				</div>
			</Container>
		);
	}
}
export default withStyles(styles)(Login);
