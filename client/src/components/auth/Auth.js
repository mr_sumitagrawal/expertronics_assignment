import React from "react";
import "../../App.css";
import { withStyles } from "@material-ui/core";
import Login from "./Login";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import Typography from "@material-ui/core/Typography";
import Box from "@material-ui/core/Box";
import Link from "@material-ui/core/Link";

const styles = theme => ({
	menuButton: {
		marginRight: theme.spacing(1)
	},
	button: {
		marginRight: theme.spacing(1)
	},
	instructions: {
		marginTop: theme.spacing(1),
		marginBottom: theme.spacing(1)
	}
});

class Auth extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			component: <Login onLogin={this.handleOnLogin} />
		};
		this.handleOnLogin = this.handleOnLogin.bind();
		this.handleDoLogin = this.handleDoLogin.bind();
	}

	handleOnLogin = res => {
		console.log("handleOnLogin: " + res);
		this.props.onComplete(res);
	};

	handleDoLogin = res => {
		console.log("handleOnRegister: " + res);
		this.setState({
			component: (
				<Login
					onForgotPassword={this.handleDoForgotPassword}
					onLogin={this.handleOnLogin}
				/>
			)
		});
	};

	render() {
		return (
			<div className="App">
				<header className="App-header">
					<AppBar color="primary" position="fixed">
						<Toolbar>
							<Typography variant="title" color="inherit" align="center">
								Expertrons Dashboard
							</Typography>
						</Toolbar>
					</AppBar>
				</header>
				<main className="App-main">{this.state.component}</main>
				<footer className="App-footer">
					<Box mt={2} mb={2}>
						<Typography variant="body2" color="textSecondary" align="center">
							{"All rights reserved at "}
							<br />
							<Link
								color="inherit"
								href="https://in.linkedin.com/in/er-sumit-agrawal"
							>
								Sumit S. Agrawal
							</Link>
						</Typography>
					</Box>
				</footer>
			</div>
		);
	}
}
export default withStyles(styles)(Auth);
