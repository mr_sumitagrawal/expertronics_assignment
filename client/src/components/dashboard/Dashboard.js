import React from "react";
import { withCookies } from "react-cookie";
import "../../App.css";
import clsx from "clsx";
import Drawer from "@material-ui/core/Drawer";
import AppBar from "@material-ui/core/AppBar";
import Toolbar from "@material-ui/core/Toolbar";
import List from "@material-ui/core/List";
import CssBaseline from "@material-ui/core/CssBaseline";
import Typography from "@material-ui/core/Typography";
import Divider from "@material-ui/core/Divider";
import IconButton from "@material-ui/core/IconButton";
import MenuIcon from "@material-ui/icons/Menu";
import ChevronLeftIcon from "@material-ui/icons/ChevronLeft";
import Card from "@material-ui/core/Card";
import CardHeader from "@material-ui/core/CardHeader";
import CardActions from "@material-ui/core/CardActions";
import Avatar from "@material-ui/core/Avatar";
import Button from "@material-ui/core/Button";
import AccountCircle from "@material-ui/icons/AccountCircle";
import { red } from "@material-ui/core/colors";
import Popover from "@material-ui/core/Popover";
import withStyles from "@material-ui/core/styles/withStyles";
import Sidebar from "./Sidebar";
import { Router, Route } from "react-router-dom";
import Home from "./pages/Home";
import History from "../commons/History";
import Users from "./list/Users";
import Tasks from "./list/Tasks";
import Profile from "./pages/Profile";

const drawerWidth = 210;
const styles = theme => ({
	theme: theme,
	root: {
		display: "flex"
	},
	appBar: {
		zIndex: theme.zIndex.drawer + 1,
		transition: theme.transitions.create(["width", "margin"], {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.leavingScreen
		})
	},
	appBarShift: {
		marginLeft: drawerWidth,
		width: `calc(100% - ${drawerWidth}px)`,
		transition: theme.transitions.create(["width", "margin"], {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.enteringScreen
		})
	},
	menuButton: {
		marginRight: 36
	},
	avatar: {
		backgroundColor: red[500]
	},
	hide: {
		display: "none"
	},
	drawer: {
		width: drawerWidth,
		flexShrink: 0,
		whiteSpace: "nowrap"
	},
	drawerOpen: {
		width: drawerWidth,
		transition: theme.transitions.create("width", {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.enteringScreen
		})
	},
	drawerClose: {
		transition: theme.transitions.create("width", {
			easing: theme.transitions.easing.sharp,
			duration: theme.transitions.duration.leavingScreen
		}),
		overflowX: "hidden",
		width: theme.spacing(7) + 1,
		[theme.breakpoints.up("sm")]: {
			width: theme.spacing(9) + 1
		}
	},
	toolbar: {
		display: "flex",
		alignItems: "center",
		justifyContent: "flex-end",
		padding: "0 8px",
		...theme.mixins.toolbar
	},
	userMenuButton: {
		marginRight: theme.spacing(2)
	},
	content: {
		flexGrow: 1,
		padding: theme.spacing(3)
	}
});

class Dashboard extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			component: "",
			open: false,
			router: "",
			anchorEl: null,
			openProfile: false,
			email: this.props.cookies.cookies.email,
			token: this.props.cookies.cookies.token,
			type: this.props.cookies.cookies.type
		};
		console.log(this.props.cookies);
		this.handleDrawerOpen = this.handleDrawerOpen.bind();
		this.handleDrawerClose = this.handleDrawerClose.bind();
	}

	componentDidMount() {
		var actions = [];
		actions.push(<Route exact path="/" component={Home} />);
		actions.push(<Route path={"/tasks"} component={Tasks} />);
		if (this.state.type == "admin") {
			actions.push(<Route path={"/users"} component={Users} />);
		} else {
			actions.push(<Route path={"/profile"} component={Profile} />);
		}
		console.log(actions);
		this.setState({
			router: <Router history={History}>{actions}</Router>
		});
	}

	handleDrawerOpen = event => {
		this.setState({ open: true });
	};

	handleDrawerClose = event => {
		this.setState({ open: false });
	};

	render() {
		const { classes } = this.props;
		return (
			<div className={classes.root}>
				<CssBaseline />
				<AppBar
					position="fixed"
					color="primary"
					className={clsx(classes.appBar, {
						[classes.appBarShift]: this.state.open
					})}
				>
					<Toolbar>
						<IconButton
							color="inherit"
							aria-label="Open drawer"
							onClick={this.handleDrawerOpen}
							edge="start"
							className={clsx(classes.menuButton, {
								[classes.hide]: this.state.open
							})}
						>
							<MenuIcon />
						</IconButton>
						<Typography variant="h6" noWrap>
							Expertrons Dashboard
						</Typography>
						<IconButton
							style={{
								position: "absolute",
								top: 10,
								right: 10
							}}
							className={classes.userMenuButton}
							aria-label="account of current user"
							aria-controls="menu-appbar"
							aria-haspopup="true"
							onClick={event => {
								this.setState({ anchorEl: event.currentTarget });
								this.setState({ openProfile: true });
							}}
							color="inherit"
						>
							<AccountCircle />
						</IconButton>

						<Popover
							id={"simple-popover"}
							open={this.state.openProfile}
							anchorEl={this.state.anchorEl}
							onClose={event => {
								this.setState({ anchorEl: null });
								this.setState({ openProfile: false });
							}}
							anchorOrigin={{
								vertical: "bottom",
								horizontal: "left"
							}}
							transformOrigin={{
								vertical: "top",
								horizontal: "center"
							}}
						>
							<Card className={classes.card}>
								<CardHeader
									avatar={
										<Avatar aria-label="user" className={classes.avatar}>
											S
										</Avatar>
									}
									title={"Hello " + this.props.cookies.cookies.email}
								/>
								<CardActions>
									<Button
										size="small"
										color="primary"
										onClick={event => {
											console.log("Logout clicked");
											window.location.reload();
											this.props.cookies.remove("token", "/");
											this.props.cookies.remove("type", "/");
										}}
									>
										Logout
									</Button>
								</CardActions>
							</Card>
						</Popover>
					</Toolbar>
				</AppBar>
				<Drawer
					variant="permanent"
					className={clsx(classes.drawer, {
						[classes.drawerOpen]: this.state.open,
						[classes.drawerClose]: !this.state.open
					})}
					classes={{
						paper: clsx({
							[classes.drawerOpen]: this.state.open,
							[classes.drawerClose]: !this.state.open
						})
					}}
					open={this.state.open}
				>
					<div className={classes.toolbar}>
						<IconButton onClick={this.handleDrawerClose}>
							{<ChevronLeftIcon />}
						</IconButton>
					</div>
					<Divider />
					<List>
						<Sidebar />
					</List>
					<Divider />
				</Drawer>
				<div className={classes.content}>
					<div className={classes.toolbar} />
					{this.state.router}
				</div>
			</div>
		);
	}
}
export default withStyles(styles)(withCookies(Dashboard));
