import React from "react";
import History from "../../commons/History";
import { withCookies } from "react-cookie";
import MaterialTable from "material-table";
import Card from "@material-ui/core/Card";
import { Grid } from "shineout";
import withStyles from "@material-ui/core/styles/withStyles";
const styles = theme => ({
	theme: theme,
	root: {
		display: "flex"
	}
});

class Home extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			component: "",
			open: false,
			data: [],
			// data: { brand: [], model: [], mboard: [] },
			name: this.props.cookies.cookies.name,
			token: this.props.cookies.cookies.token
		};
		console.log(this.props.cookies);
	}

	componentDidMount() {}

	render() {
		const { classes } = this.props;
		return (
			<React.Fragment className={classes.root}>
				<h3>Expertrons Home page</h3>
			</React.Fragment>
		);
	}
}
export default withStyles(styles)(withCookies(Home));
