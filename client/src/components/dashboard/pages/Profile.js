import React from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import { get, update } from "../network/Users";
import { withCookies } from "react-cookie";
import { Card, Message, Input, Rule, Form, Grid } from "shineout";

const rules = Rule();
const styles = theme => ({
	theme: theme,
	root: {
		display: "wrap"
	},
	container: {
		display: "block",
		flexWrap: "wrap"
	},
	fab: {
		margin: theme.spacing(1)
	},
	box: {
		component: "div",
		display: "inline",
		flexWrap: "wrap"
	},
	card: {
		padding: theme.spacing(1),
		margin: theme.spacing(2)
	},
	textField: {
		margin: 8,
		padding: theme.spacing(1),
		width: 200
	},
	media: {
		paddingTop: "56.25%"
	}
});

class Profile extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			data: [],
			email: this.props.cookies.cookies.email,
			token: this.props.cookies.cookies.token,
			type: this.props.cookies.cookies.type,
			value: { email: this.props.cookies.cookies.email }
		};
	}

	componentWillMount() {
		get(this.state.token, { email: this.state.email }, result => {
			if (result.length > 0) this.setState({ value: result[0] });
		});
	}

	render() {
		return (
			<React.Fragment>
				<Card fullWidth>
					<Card.Body>
						<h2>Expert Profile</h2>
						<Form
							value={this.state.value}
							onSubmit={value => {
								console.log(value);
								value.type = this.state.type;
								update(
									this.state.token,
									{ email: value.email },
									value,
									result => {
										if (result) {
											Message.success("Profile updated", 3, {
												position: "bottom-right"
											});
											this.setState(value);
										} else {
											Message.error("Failed to update profile. Try again", 3, {
												position: "bottom-right"
											});
										}
									}
								);
							}}
						>
							<Grid gutter={2}>
								<Grid width={1 / 2}>
									<Form.Item required label="Name">
										<Input
											name="name"
											fullWidth
											rules={[rules.required, rules.isExist]}
										/>
									</Form.Item>
								</Grid>
								<Grid width={1 / 2}>
									<Form.Item required label="Email">
										<Input
											name="email"
											disabled
											fullWidth
											rules={[rules.required, rules.isExist]}
										/>
									</Form.Item>
								</Grid>
								<Grid width={1 / 2}>
									<Form.Item label="Mobile">
										<Input name="mobile" fullWidth />
									</Form.Item>
								</Grid>
								<Grid width={1 / 2}>
									<Form.Item label="Date of Birth">
										<Input name="dob" fullWidth />
									</Form.Item>
								</Grid>
								<Grid width={1 / 2}>
									<Form.Item label="Gender">
										<Input name="gender" fullWidth />
									</Form.Item>
								</Grid>
								<Grid width={1 / 2}>
									<Form.Item label="Home town">
										<Input name="home_town" fullWidth />
									</Form.Item>
								</Grid>
								<Grid width={1 / 2}>
									<Form.Item label="Job sector">
										<Input name="job_sector" fullWidth />
									</Form.Item>
								</Grid>
								<Grid width={1 / 2}>
									<Form.Item label="Company name">
										<Input name="company_name" fullWidth />
									</Form.Item>
								</Grid>
								<Grid width={1 / 2}>
									<Form.Item label="Designation">
										<Input name="designation" fullWidth />
									</Form.Item>
								</Grid>
								<Grid width={1 / 2}>
									<Form.Item label="Posting place">
										<Input name="posting_place" fullWidth />
									</Form.Item>
								</Grid>
								<Grid width={1 / 2}>
									<Form.Item label="Current CTC">
										<Input name="ctc" fullWidth />
									</Form.Item>
								</Grid>
								<Grid width={1 / 2}>
									<Form.Item label="Shortlisted companies">
										<Input name="shortlisted_ccompanies" fullWidth />
									</Form.Item>
								</Grid>
								<Grid width={1 / 2}>
									<Form.Item label="Internship Experience">
										<Input name="internship_experience" fullWidth />
									</Form.Item>
								</Grid>
								<Grid width={1 / 2}>
									<Form.Item label="College name">
										<Input name="college_name" fullWidth />
									</Form.Item>
								</Grid>
								<Grid width={1 / 2}>
									<Form.Item label="Programme">
										<Input name="programme" fullWidth />
									</Form.Item>
								</Grid>
								<Grid width={1 / 2}>
									<Form.Item label="Department">
										<Input name="department" fullWidth />
									</Form.Item>
								</Grid>
								<Grid width={1 / 2}>
									<Form.Item label="CGPA">
										<Input name="cgpa" fullWidth />
									</Form.Item>
								</Grid>
								<Grid width={1 / 2}>
									<Form.Item label="SSC %">
										<Input name="ssc" fullWidth />
									</Form.Item>
								</Grid>
								<Grid width={1 / 2}>
									<Form.Item label="HSC %">
										<Input name="hsc" fullWidth />
									</Form.Item>
								</Grid>
								<Grid width={1 / 2}>
									<Form.Item label="Academic project">
										<Input name="academic_project" fullWidth />
									</Form.Item>
								</Grid>
								<Grid width={1 / 2}>
									<Form.Item label="Leadership roles">
										<Input name="leadership_roles" fullWidth />
									</Form.Item>
								</Grid>
								<Grid width={1 / 2}>
									<Form.Item label="Extra curricular">
										<Input name="extra_curricular" fullWidth />
									</Form.Item>
								</Grid>
								<Grid width={1 / 2}>
									<Form.Item label="Self description in 3 words">
										<Input name="description" fullWidth />
									</Form.Item>
								</Grid>
								<Grid width={1 / 2}>
									<Form.Item label="LinkedIn Profile">
										<Input name="linkedin_profile" fullWidth />
									</Form.Item>
								</Grid>
							</Grid>
						</Form>
						<Card.Submit
							style={{
								marginTop: 20,
								width: "100%"
							}}
						>
							Submit
						</Card.Submit>
					</Card.Body>
				</Card>
			</React.Fragment>
		);
	}
}

export default withStyles(styles)(withCookies(Profile));
