import React from "react";
import "../../App.css";
import { withCookies } from "react-cookie";
import List from "@material-ui/core/List";
import ListItem from "@material-ui/core/ListItem";
import ListItemIcon from "@material-ui/core/ListItemIcon";
import ListItemText from "@material-ui/core/ListItemText";
import withStyles from "@material-ui/core/styles/withStyles";
import History from "../commons/History";
const styles = theme => ({
	theme: theme,
	root: {
		display: "flex"
	}
});

class Sidebar extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			component: "",
			open: false,
			menu: [],
			email: this.props.cookies.cookies.email,
			token: this.props.cookies.cookies.token,
			type: this.props.cookies.cookies.type
		};
		this.handleItemClick = this.handleItemClick.bind();
	}
	componentDidMount() {
		var actions = [];
		actions.push({
			text: "Home",
			url: "/",
			icon: (
				<img
					src={"./icons/home.svg"}
					width={"24px"}
					height={"24px"}
					alt={"HOME"}
				/>
			)
		});
		actions.push({
			text: "Tasks",
			url: "/tasks",
			icon: (
				<img
					src={"./icons/tasks.svg"}
					width={"24px"}
					height={"24px"}
					alt={"TASKS"}
				/>
			)
		});
		if (this.state.type == "admin") {
			actions.push({
				text: "Users",
				url: "/users",
				icon: (
					<img
						src={"./icons/users.svg"}
						width={"24px"}
						height={"24px"}
						alt={"USERS"}
					/>
				)
			});
		} else {
			actions.push({
				text: "Profile",
				url: "/profile",
				icon: (
					<img
						src={"./icons/profile.svg"}
						width={"24px"}
						height={"24px"}
						alt={"PROFILE"}
					/>
				)
			});
		}
		console.log(actions);
		this.setState({ menu: actions });
	}

	handleItemClick = url => event => {
		this.setState({ url: url });
		console.log(url);
		History.push(url);
	};

	render() {
		const { classes } = this.props;
		return (
			<div className={classes.root}>
				<List>
					{this.state.menu.map(item => (
						<ListItem
							button
							key={item.text}
							onClick={this.handleItemClick(item.url)}
						>
							<ListItemIcon>{item.icon}</ListItemIcon>
							<ListItemText primary={item.text} />
						</ListItem>
					))}
				</List>
			</div>
		);
	}
}
export default withStyles(styles)(withCookies(Sidebar));
