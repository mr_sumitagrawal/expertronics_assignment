import React from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import MaterialTable from "material-table";
import { create, update, delet, get } from "../network/Tasks";
import { get as getEmails } from "../network/Users";
import { withCookies } from "react-cookie";
import history from "../../commons/History";
import { Message } from "shineout";

const styles = theme => ({
	theme: theme,
	root: {
		display: "wrap"
	},
	container: {
		display: "block",
		flexWrap: "wrap"
	},
	fab: {
		margin: theme.spacing(1)
	},
	box: {
		component: "div",
		display: "inline",
		flexWrap: "wrap"
	},
	card: {
		padding: theme.spacing(1),
		margin: theme.spacing(2)
	},
	textField: {
		margin: 8,
		padding: theme.spacing(1),
		width: 200
	},
	media: {
		paddingTop: "56.25%"
	}
});

class TVList extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			columns: [],
			data: [],
			email: this.props.cookies.cookies.email,
			token: this.props.cookies.cookies.token,
			type: this.props.cookies.cookies.type,
			emails: []
		};
	}

	componentDidMount() {
		get(
			this.state.token,
			{
				expert_email: this.state.type != "admin" ? this.state.email : undefined
			},
			data => {
				if (data != null) {
					this.setState({ data: data });
				}
			}
		);
		var columns = [
			{ title: "Name", field: "name" },
			{ title: "Task", field: "task" }
		];
		if (this.state.type == "admin")
			getEmails(this.state.token, {}, result => {
				var emails = {};
				result.map(user => {
					emails[user.email] = user.name;
				});
				columns.push({
					title: "Expert",
					field: "expert_email",
					lookup: emails
				});
				this.setState({ columns: columns });
			});
		else {
			columns.push({ title: "Expert", field: "expert_email" });
			this.setState({ columns: columns });
		}
	}

	render() {
		const { classes } = this.props;
		return (
			<div className={classes.root}>
				<MaterialTable
					title={"Tasks"}
					columns={this.state.columns}
					data={this.state.data}
					options={{
						filtering: true,
						grouping: true,
						addRowPosition: "first",
						pageSize: 20,
						pageSizeOptions: [20, 50, 100, 150, 200],
						actionsColumnIndex: 100
					}}
					editable={{
						onRowAdd:
							this.state.type == "admin"
								? newData =>
										new Promise(resolve => {
											create(this.state.token, newData, result => {
												if (result)
													get(this.state.token, {}, data => {
														this.setState({ data: data });
														Message.success("Task created", 3, {
															position: "bottom-right"
														});
														resolve();
													});
												else {
													Message.error("Failed to create Task. Try again", 3, {
														position: "bottom-right"
													});
													resolve();
												}
											});
										})
								: undefined,
						onRowUpdate:
							this.state.type == "admin"
								? (newData, oldData) =>
										new Promise(resolve => {
											update(this.state.token, oldData, newData, result => {
												if (result)
													get(this.state.token, {}, data => {
														this.setState({ data: data });
														Message.success("Task updated", 3, {
															position: "bottom-right"
														});
														resolve();
													});
												else {
													Message.error("Failed to delete task. Try again", 3, {
														position: "bottom-right"
													});
													resolve();
												}
											});
										})
								: undefined,
						onRowDelete:
							this.state.type == "admin"
								? oldData =>
										new Promise(resolve => {
											delet(this.state.token, oldData.name, result => {
												if (result)
													get(this.state.token, {}, data => {
														this.setState({ data: data });
														Message.success("Task deleted", 3, {
															position: "bottom-right"
														});
														resolve();
													});
												else {
													Message.error(
														"Failed to delete tasks. Try again",
														3,
														{
															position: "bottom-right"
														}
													);
													resolve();
												}
											});
										})
								: undefined
					}}
				/>
			</div>
		);
	}
}

export default withStyles(styles)(withCookies(TVList));
