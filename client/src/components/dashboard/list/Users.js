import React from "react";
import withStyles from "@material-ui/core/styles/withStyles";
import MaterialTable from "material-table";
import { create, update, get, delet } from "../network/Users";
import { withCookies } from "react-cookie";
import { Message } from "shineout";

const styles = theme => ({
	theme: theme,
	root: {
		display: "wrap"
	},
	container: {
		display: "block",
		flexWrap: "wrap"
	},
	fab: {
		margin: theme.spacing(1)
	},
	box: {
		component: "div",
		display: "inline",
		flexWrap: "wrap"
	},
	card: {
		padding: theme.spacing(1),
		margin: theme.spacing(2)
	},
	textField: {
		margin: 8,
		padding: theme.spacing(1),
		width: 200
	},
	media: {
		paddingTop: "56.25%"
	}
});

class Users extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			columns: [
				{ title: "Name", field: "name" },
				{ title: "Email", field: "email" },
				{
					title: "Password",
					field: "temp_password",
					emptyValue: "***********************"
				},
				{
					title: "Type",
					field: "type",
					lookup: { admin: "Admin", expert: "Expert" }
				}
			],
			data: [],
			email: this.props.cookies.cookies.email,
			token: this.props.cookies.cookies.token,
			type: this.props.cookies.cookies.type
		};
	}

	componentDidMount() {
		get(this.state.token, {}, data => {
			console.log(data);
			this.setState({ data: data });
		});
	}

	render() {
		const { classes } = this.props;
		return (
			<div className={classes.root}>
				<MaterialTable
					title={"Users"}
					columns={this.state.columns}
					data={this.state.data}
					options={{
						addRowPosition: "first",
						actionsColumnIndex: 100
					}}
					editable={{
						onRowAdd: newData =>
							new Promise(resolve => {
								newData.password = newData.temp_password;
								newData.temp_password = "***********************";
								create(this.state.token, newData, result => {
									if (result)
										get(this.state.token, {}, data => {
											this.setState({ data: data });
											Message.success("Action created", 3, {
												position: "bottom-right"
											});
											resolve();
										});
									else {
										Message.error("Failed to create action. Try again", 3, {
											position: "bottom-right"
										});
										resolve();
									}
								});
							}),
						onRowUpdate: (newData, oldData) =>
							new Promise(resolve => {
								newData.password = newData.temp_password;
								newData.temp_password = "***********************";
								update(this.state.token, oldData, newData, result => {
									if (result)
										get(this.state.token, {}, data => {
											this.setState({ data: data });
											Message.success("Action updated", 3, {
												position: "bottom-right"
											});
											resolve();
										});
									else {
										Message.error("Failed to delete action. Try again", 3, {
											position: "bottom-right"
										});
										resolve();
									}
								});
							}),
						onRowDelete: oldData =>
							new Promise(resolve => {
								delet(this.state.token, oldData.name, result => {
									if (result)
										get(this.state.token, {}, data => {
											this.setState({ data: data });
											Message.success("Action deleted", 3, {
												position: "bottom-right"
											});
											resolve();
										});
									else {
										Message.error("Failed to delete action. Try again", 3, {
											position: "bottom-right"
										});
										resolve();
									}
								});
							})
					}}
				/>
			</div>
		);
	}
}

export default withStyles(styles)(withCookies(Users));
