import superagent from "superagent";

const URL = "/api/v1/users";

export const create = (token, users_body, callback) => {
	superagent
		.post(URL)
		.set("Authorization", "Bearer " + token)
		.send(users_body)
		.end((err, response) => {
			if (err) {
				callback(false);
				console.log("Request failed");
			} else {
				if (response.status == 200) {
					callback(true);
				} else {
					callback(false);
				}
			}
			console.log(response);
		});
};

export const get = (token, query, callback) => {
	superagent
		.get(URL)
		.set("Authorization", "Bearer " + token)
		.query(query)
		.end((err, response) => {
			if (err) {
				callback([]);
				console.log("Request failed");
			} else {
				if (response.status == 200) {
					callback(response.body);
				} else {
					callback([]);
					console.log("No data available");
				}
			}
			console.log(response);
		});
};

export const update = (token, oldValue, newValue, callback) => {
	newValue.old_email = oldValue.email;
	superagent
		.put(URL)
		.set("Authorization", "Bearer " + token)
		.send(newValue)
		.end((err, response) => {
			if (err) {
				callback([false]);
				console.log("Request failed");
			} else {
				if (response.status == 200) {
					callback(true);
				} else {
					callback(false);
				}
			}
			console.log(response);
		});
};

export const delet = (token, name, callback) => {
	superagent
		.delete(URL)
		.set("Authorization", "Bearer " + token)
		.send({ name: name })
		.end((err, response) => {
			if (err) {
				callback([false]);
				console.log("Request failed");
			} else {
				if (response.status == 200) {
					callback(true);
				} else {
					callback(false);
				}
			}
			console.log(response);
		});
};
