import React from "react";
import "./App.css";
import Auth from "./components/auth/Auth";
import { withCookies } from "react-cookie";
import Dashboard from "./components/dashboard/Dashboard";

class App extends React.Component {
	constructor(props) {
		super(props);
		this.state = {
			content: <Auth onComplete={this.handleOnAuth} />
		};
		this.handleOnAuth = this.handleOnAuth.bind();
	}
	componentDidMount() {
		if (this.props.cookies.cookies.token !== undefined) {
			this.setState({ content: <Dashboard cookies={this.props.cookies} /> });
		} else {
			this.setState({ content: <Auth onComplete={this.handleOnAuth} /> });
		}
	}

	handleOnAuth = res => {
		this.props.cookies.set("email", res.email, {
			path: "/",
			maxAge: 3600
		});
		this.props.cookies.set("token", res.token, {
			path: "/",
			maxAge: 3600
		});
		this.props.cookies.set("type", res.type, {
			path: "/",
			maxAge: 3600
		});

		this.setState({ content: <Dashboard /> });
	};

	render() {
		return <div className="App">{this.state.content}</div>;
	}
}
export default withCookies(App);
