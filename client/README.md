# Cloudwalker Streaming Technologies PVT. LTD. 
## OTA - 2.0  (Frontend)
## Installation
This is frontend repository part of OTA project. Clone ota_server & ota_api repositories to deploy stack
Frontend only can be tested by following process 
## Prerequisites:
    Node.Js version 8+
    ota_client repository clone
## Commands
    $ cd ./ota_client
    $ npm install
    $ npm start
