# Expertrons Hiring assignment solution

## Created by: Sumit S. Agrawal

## Steps to execute project

### Building Client
    $ cd client
    $ npm install
    $ npm run build
    $ mkdir ../server/public
    $ mv build/* ../server/public/
    
### Required .env contents

    MONGO_DB=expertrons_db
    MONGO_PORT=27017
    PORT=10000
    SECRET= < ANY_RANDON_STRING >

### Execute following commands

    $ cd server
    $ docker-compose up -d
    $ docker-compose exec -T mongo sh -c 'mongorestore --archive' < db.dump
